import pygame


pygame.init()
win=pygame.display.set_mode((1000, 1000)) 

pygame.display.set_caption("Dimachit Game")
#важные переменные
clock = pygame.time.Clock()

FPS = 120
sek = 0
T = True
S = 0
kolvokadr = 5

MaxHp = 250
x = 50
y = 760
hp = MaxHp
xw = 0
yw = 0
width = 108
height = 140
speed = 5
vWall = 5

jump = False
jump_count = 10

left = False
right = False
animCount = 0
animAnim = 0
lastMove = "right"
bullets = []
max_kolvo_snaryads = 5
TimeWolf = 4
pogr = 50

HPText = pygame.font.SysFont('serif', 70)


distAttack = 5

#картиночки
walkRight = [pygame.image.load('sprites/right1.png'), pygame.image.load('sprites/right2.png'), pygame.image.load('sprites/right3.png'),
pygame.image.load('sprites/right4.png'), pygame.image.load('sprites/right5.png'),
pygame.image.load('sprites/right6.png'), pygame.image.load('sprites/right7.png'), pygame.image.load('sprites/right8.png')]

walkLeft = [pygame.image.load('sprites/left1.png'), pygame.image.load('sprites/left2.png'), pygame.image.load('sprites/left3.png'),
pygame.image.load('sprites/left4.png'), pygame.image.load('sprites/left5.png'),
pygame.image.load('sprites/left6.png'), pygame.image.load('sprites/left7.png'), pygame.image.load('sprites/left8.png')]

AnimRight = [pygame.image.load('sprites/wolfRight1.png'), pygame.image.load('sprites/wolfRight2.png'), pygame.image.load('sprites/wolfRight3.png'),
pygame.image.load('sprites/wolfRight4.png'), pygame.image.load('sprites/wolfRight5.png'),
pygame.image.load('sprites/wolfRight6.png')]

AnimLeft = [pygame.image.load('sprites/wolfLeft1.png'), pygame.image.load('sprites/wolfLeft2.png'), pygame.image.load('sprites/wolfLeft3.png'),
pygame.image.load('sprites/wolfLeft4.png'), pygame.image.load('sprites/wolfLeft5.png'),
pygame.image.load('sprites/wolfLeft6.png')]

AttackingL = [pygame.image.load('sprites/attack_1.png'), pygame.image.load('sprites/attack_2.png'), pygame.image.load('sprites/attack_3.png'),
pygame.image.load('sprites/attack_4.png'), pygame.image.load('sprites/attack_5.png')]

AttackingR = [pygame.image.load('sprites/Rattack_1.png'), pygame.image.load('sprites/Rattack_2.png'), pygame.image.load('sprites/Rattack_3.png'),
pygame.image.load('sprites/Rattack_4.png'), pygame.image.load('sprites/Rattack_5.png')]
wolfs = []



bg = pygame.image.load('sprites/bgnorm.jpg')
playerStandLeft = pygame.image.load('sprites/left1.png')
playerStandRight = pygame.image.load('sprites/right1.png')
aim = pygame.image.load('sprites/aimp.png')

screen = pygame.Surface((1000, 1000))

class Menu:
    def __init__(self, punkts = [100, 200, 'Punkt', (250,250,30), (250,30,250), 0]):
        self.punkts = punkts
    def drawMenu(self, poverhnost, font, Npunkt):
        for i in self.punkts:
            if Npunkt == i[5]:
                poverhnost.blit(font.render(i[2], 1, i[4]), (i[0],i[1]))
            else:
                poverhnost.blit(font.render(i[2], 1, i[3]), (i[0],i[1]))
    def menu(self):
        global hp, wolfs, bullets, MaxHp
        go = True
        fontMenu = pygame.font.SysFont('serif', 50)
        punkt = 0
        if hp <= 0:
            wolfs = []
            bullets = []
            hp = MaxHp
        while go:
            screen.fill((0,100,200))
            mp = pygame.mouse.get_pos()
            for i in self.punkts:
                if mp[0] > i[0] and mp[0] < i[0]+155 and mp[1] > i[1] and mp[1] < i[1]+50:
                    punkt = i[5]
            self.drawMenu(screen, fontMenu, punkt)


            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit() #sys.exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        pygame.quit() #sys.exit()

                    if event.key == pygame.K_UP:
                        if punkt > 0:
                            punkt -= 1


                    if event.key == pygame.K_DOWN:
                        if punkt < len(self.punkts)-1:
                            punkt += 1

                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    if punkt == 0:
                        go = False
                        pygame.mouse.set_visible(False)
                    elif punkt == 1:
                        pygame.quit() #sys.exit()

            win.blit(screen,(0,0))
            pygame.display.update()
                    
                    
            


punkts = [(420, 250, 'Game', (250,250,30), (250,30,250), 0),
          (420, 300, 'Quit', (250,250,30), (250,30,250), 1)]
game = Menu(punkts)
game.menu()


class Snaryad():
    def __init__(self, x, y, radius, color, vectorX, vectorY):
        self.x = x
        self.y = y
        self.radius = radius
        self.color = color
        self.vectorX = vectorX
        self.vectorY = vectorY
        self.v = 25
    def draw(self, win):
        pygame.draw.circle(win,self.color, (self.x, self.y), self.radius)


class Animal():
    def __init__(self, X, Y, HP, vektor, attack):
        self.X = X
        self.Y = Y
        self.vektor = vektor
        self.v = 3
        self.HP = HP
        self.attack = attack
    def drawAnimals(self, win):
        global animAnim
        if animAnim +1 >= kolvokadr*6:
            animAnim = 0
            self.attack = False
        if self.vektor == -1 and not self.attack:
            win.blit(AnimLeft[animAnim // kolvokadr], (self.X,self.Y))
            animAnim += 1
        if self.vektor == 1 and not self.attack:
            win.blit(AnimRight[animAnim // kolvokadr], (self.X,self.Y))
            animAnim += 1
        if self.attack:
            if self.vektor == -1:
                win.blit(AttackingL[animAnim // (kolvokadr*6 // 5)], (self.X, self.Y))
                animAnim += 1
            if self.vektor == 1:
                win.blit(AttackingR[animAnim // (kolvokadr*6 // 5)], (self.X, self.Y))
                animAnim += 1


        
      
def drawWindow():
    global animCount, xw    
    win.blit(bg, (xw,yw))
    win.blit(HPText.render('HP:' + str(hp), 1, (165, 56, 56)), (400, 100))


    
    
    if animCount +1 >= kolvokadr*8:
        animCount = 0
    if hp > 0:
        if left:
            win.blit(walkLeft[animCount // kolvokadr], (x,y))
            animCount += 1
            if xw < 0:
                xw += vWall
        elif right:
            win.blit(walkRight[animCount // kolvokadr], (x,y))
            animCount += 1
            if xw > -335:
                xw -= vWall
        else:
            if lastMove == 'right':
                win.blit(playerStandRight, (x,y))
            else:
                win.blit(playerStandLeft, (x,y))

    for wolf in wolfs:
        wolf.drawAnimals(win)
            
    for bullet in bullets:
        bullet.draw(win)
    if hp > 0:
        win.blit(aim, (pos[0]-50, pos[1]-50))
    
    pygame.display.update()

run = True
while run:

    clock.tick(FPS)
    sek += 1
    if sek + 1 > FPS*2*TimeWolf:
        sek = 0
    if not T:
        S += 1
        if S >= FPS*0.5:
            S = 0
            T = True


    if pygame.mouse.get_focused():
        pos = pygame.mouse.get_pos()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
            pygame.quit()
            
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 and hp > 0:

            if pos[0] > x + width // 2:
                lastMove = "right"
            else:
                lastMove = "left"
            if pos[0] != x + width // 2 or pos[1] != y + height // 2:
                cos = (pos[0] - (x + width // 2))/((pos[0] - (x + width // 2))**2 + (pos[1]-(y + height // 2))**2)**0.5
                sin = (pos[1]-(y + height // 2))/((pos[0] - (x + width // 2))**2 + (pos[1]-(y + height // 2))**2)**0.5
            vectorX = int(10*cos)
            vectorY = int(10*sin)
            if len(bullets) < max_kolvo_snaryads:
                bullets.append(Snaryad(round(x + width // 2),round(y + height // 2), 5, (255, 0, 0), vectorX, vectorY))


    if sek == 1:
        wolfs.append(Animal(1050, 740, 100, -1, False))

    if sek == FPS*TimeWolf:
        wolfs.append(Animal(-300, 740, 100, 1, False))
        
    for wolf in wolfs:
        if wolf.X > -400 and wolf.X < 1100: #and not wolf.attack:
            wolf.X += wolf.v * wolf.vektor
            if wolf.vektor == -1 and wolf.X + 238 + distAttack  < x:
                wolf.vektor = 1

            if wolf.vektor == 1 and wolf.X > x + width + distAttack:
                wolf.vektor = -1




            if (wolf.vektor == -1 and wolf.X < x + width and wolf.X > x) or\
                    (wolf.vektor == 1 and wolf.X + 238 + distAttack > x and wolf.X + 238 < x + width) and not wolf.attack:
                wolf.attack = True
            if wolf.attack and y <= 760 and y >= 760 - height and T and hp > 0:
                hp -= 50
                T = False
            if hp <= 0:
                hp = 250
                pygame.mouse.set_visible(True)
                game.menu()



    for bullet in bullets:
        if bullet.x > 0 and bullet.x < 1000 and bullet.y > 0 and bullet.y < 1000:
            bullet.x += bullet.vectorX
            bullet.y += bullet.vectorY
        else:
            bullets.pop(bullets.index(bullet))
        
        if wolf in wolfs and bullet.x > wolf.X + pogr and bullet.x <wolf.X + 238 - pogr and bullet.y > wolf.Y + pogr and bullet.y < wolf.Y + 198 - pogr:
            bullets.pop(bullets.index(bullet))
            wolf.HP -= 7
        if wolf.HP <= 0 and wolf in wolfs:
            wolfs.pop(wolfs.index(wolf))
            #del (wolf)
                

    

        

    keys = pygame.key.get_pressed()

    
        
    if keys[pygame.K_ESCAPE]:
        pygame.mouse.set_visible(True)
        game.menu()

    if keys[pygame.K_a] and hp > 0:
        if 5 < x:
            x -= speed
            left = True
            right = False
            lastMove = "left"
        elif x <= 5:
            left = True
            right = False
            lastMove = "left"
    elif keys[pygame.K_d] and hp > 0:
        if  x < 1000 - width - 5:
            x += speed
            right = True
            left = False
            lastMove = "right"
        elif x >= 1000 - width - 5:
            right = True
            left = False
            lastMove = "right"
    else:
        left = False
        right = False
        animCount = 0
    if not(jump):
        if keys[pygame.K_w]:
                jump = True

    else:
        if jump_count >= -10:
            if jump_count < 0:
                y += (jump_count ** 2) / 3
            else:
                y -= (jump_count ** 2) / 3
            jump_count -= 0.5

        else:
            jump = False
            jump_count = 10

    drawWindow()
