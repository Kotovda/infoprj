import pygame
import random


pygame.init()
win = pygame.display.set_mode((1671, 1000))

pygame.display.set_caption("Dimachit Game")
#важные переменные
clock = pygame.time.Clock()

FPS = 120
sek = 0
T = True
S = 0
kolvokadr = 5

MaxHp = 250
x = 50
y = 760
hp = MaxHp
xw = 0
yw = 0
width = 108
height = 70
speed = 8
damage = 9
Score = 0
scup_time = 1
b_color = (255, 200, 0)

jump = False
jump_count = 10
aaleft = False
right = False
animCount = 0
animAnim = 0
lastMove = "right"
bullets = []
max_kolvo_snaryads = 5
TimeWolf = 2
pogr = 50
last_wolf = 0
last_bonus = 0
regen_time = 0

HPText = pygame.font.SysFont('serif', 70)


distAttack = 2

#картиночки
walkRight = [pygame.image.load('sprites/right1.png'), pygame.image.load('sprites/right2.png'), pygame.image.load('sprites/right3.png'),
pygame.image.load('sprites/right4.png'), pygame.image.load('sprites/right5.png'),
pygame.image.load('sprites/right6.png'), pygame.image.load('sprites/right7.png'), pygame.image.load('sprites/right8.png')]

walkLeft = [pygame.image.load('sprites/left1.png'), pygame.image.load('sprites/left2.png'), pygame.image.load('sprites/left3.png'),
pygame.image.load('sprites/left4.png'), pygame.image.load('sprites/left5.png'),
pygame.image.load('sprites/left6.png'), pygame.image.load('sprites/left7.png'), pygame.image.load('sprites/left8.png')]

AnimRight = [pygame.image.load('sprites/wolfRight1.png'), pygame.image.load('sprites/wolfRight2.png'), pygame.image.load('sprites/wolfRight3.png'),
pygame.image.load('sprites/wolfRight4.png'), pygame.image.load('sprites/wolfRight5.png'),
pygame.image.load('sprites/wolfRight6.png')]

AnimLeft = [pygame.image.load('sprites/wolfLeft1.png'), pygame.image.load('sprites/wolfLeft2.png'), pygame.image.load('sprites/wolfLeft3.png'),
pygame.image.load('sprites/wolfLeft4.png'), pygame.image.load('sprites/wolfLeft5.png'),
pygame.image.load('sprites/wolfLeft6.png')]

AttackingL = [pygame.image.load('sprites/attack_1.png'), pygame.image.load('sprites/attack_2.png'), pygame.image.load('sprites/attack_3.png'),
pygame.image.load('sprites/attack_4.png'), pygame.image.load('sprites/attack_5.png')]

AttackingR = [pygame.image.load('sprites/Rattack_1.png'), pygame.image.load('sprites/Rattack_2.png'), pygame.image.load('sprites/Rattack_3.png'),
pygame.image.load('sprites/Rattack_4.png'), pygame.image.load('sprites/Rattack_5.png')]
wolfs = []
heals = []
dds = []
moons = []
birds = []

healsp = pygame.image.load('sprites/healsp.png')
ddsp = pygame.image.load('sprites/ddb.png')
moonsp = pygame.image.load('sprites/moonb.png')



bg = pygame.image.load('sprites/bgnorm.jpg')
playerStandLeft = pygame.image.load('sprites/left1.png')
playerStandRight = pygame.image.load('sprites/right1.png')
aim = pygame.image.load('sprites/aimp.png')

screen = pygame.Surface((1671, 1000))

class Menu:
    def __init__(self, punkts = [100, 200, 'Punkt', (250,250,30), (250,30,250), 0]):
        self.punkts = punkts
    def drawMenu(self, poverhnost, font, Npunkt):
        for i in self.punkts:
            if Npunkt == i[5]:
                poverhnost.blit(font.render(i[2], 1, i[4]), (i[0],i[1]))
            else:
                poverhnost.blit(font.render(i[2], 1, i[3]), (i[0],i[1]))
    def menu(self):
        global hp, wolfs, bullets, MaxHp
        fontMenu = pygame.font.SysFont('serif', 50)
        punkt = 0
        self.drawMenu(screen, fontMenu, punkt)
        go = True
        while go:
            screen.fill((0,100,200))
            mp = pygame.mouse.get_pos()
            for i in self.punkts:
                if mp[0] > i[0] and mp[0] < i[0]+155 and mp[1] > i[1] and mp[1] < i[1]+50:
                    punkt = i[5]
            self.drawMenu(screen, fontMenu, punkt)


            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit() #sys.exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        pygame.quit() #sys.exit()

                    if event.key == pygame.K_UP:
                        if punkt > 0:
                            punkt -= 1


                    if event.key == pygame.K_DOWN:
                        if punkt < len(self.punkts)-1:
                            punkt += 1
                    if event.key == 13:
                        if punkt == 0:
                            go = False
                            pygame.mouse.set_visible(False)
                        if punkt == 1:
                            pygame.quit()


                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    if punkt == 0 and mp[0] > punkts[0][0] and mp[0] < punkts[0][0]+155 and mp[1] > punkts[0][1] and mp[1] < punkts[0][1]+50:
                        go = False
                        pygame.mouse.set_visible(False)
                    elif punkt == 1 and mp[0] > punkts[1][0] and mp[0] < punkts[1][0]+155 and mp[1] > punkts[1][1] and mp[1] < punkts[1][1]+50:
                        pygame.quit() #sys.exit()

            win.blit(screen,(0,0))
            f = open("q.txt")
            best = f.read()
            win.blit(HPText.render('Highscore: ' + best, 1, (255, 0, 0)), (310, 500))
            f.close()

            pygame.display.update()
                    
                    
            


punkts = [(420, 250, 'Game', (250,250,30), (250,30,250), 0),
          (420, 300, 'Quit', (250,250,30), (250,30,250), 1)]
game = Menu(punkts)
game.menu()


class Snaryad():
    def __init__(self, x, y, radius, color, vectorX, vectorY, damag):
        self.x = x
        self.y = y
        self.radius = radius
        self.color = color
        self.vectorX = vectorX
        self.vectorY = vectorY
        self.v = 25
        self.damag = damag

    def draw(self, win):
        pygame.draw.circle(win,self.color, (self.x, self.y), self.radius)

class healb():
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.v = 4
    def draw(self, win):
        win.blit(healsp, (self.x, self.y))




class ddb():
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.v = 4
        self.act = False
        self.Ptime = 2*FPS
    def draw(self, win):
        if not self.act and self.y < 760:
            win.blit(ddsp, (self.x, self.y))
        elif self.act:
            win.blit(HPText.render('Double damage!', 1, (165, 56, 56)), (300, 10))

            
class moonb():
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.v = 4
        self.act = False
        self.Ptime = 3*FPS
    def draw(self, win):
        if not self.act and self.y < 760:
            win.blit(moonsp, (self.x, self.y))
        elif self.act:
            win.blit(HPText.render('Full Moon!', 1, (165, 56, 56)), (370, 300))




class Animal():
    def __init__(self, X, Y, HP, vektor, attack,animn):
        self.X = X
        self.Y = Y
        self.vektor = vektor
        self.v = 5
        self.HP = HP
        self.attack = attack
        self.animn = animn
    def drawAnimals(self, win):
        if self.animn +1 >= kolvokadr*6:
            self.animn = 0
            self.attack=False
        if self.vektor == -1 and not self.attack:
            win.blit(AnimLeft[self.animn // kolvokadr], (self.X,self.Y))
            self.animn+=1
        if self.vektor == 1 and not self.attack:
            win.blit(AnimRight[self.animn // kolvokadr], (self.X,self.Y))
            self.animn+=1
        if self.attack:
            if self.vektor == -1:
                win.blit(AttackingL[self.animn // (kolvokadr*6 // 5)], (self.X, self.Y))
                self.animn+=1
            if self.vektor == 1:
                win.blit(AttackingR[self.animn // (kolvokadr*6 // 5)], (self.X, self.Y))
                self.animn+=1



        
      
def drawWindow():
    global animCount, xw    
    win.blit(bg, (xw,yw))
    win.blit(HPText.render('HP:' + str(hp), 1, (165, 56, 56)), (400, 100))
    win.blit(HPText.render('Score:' + str(Score), 1, (165, 56, 56)), (390, 200))

    for heal in heals:
        heal.draw(win)
    for dd in dds:
        dd.draw(win)
    for moon in moons:
        moon.draw(win)

    if animCount +1 >= kolvokadr*8:
        animCount = 0
    if hp > 0:
        if left:
            win.blit(walkLeft[animCount // kolvokadr], (x,y))
            animCount += 1
        elif right:
            win.blit(walkRight[animCount // kolvokadr], (x,y))
            animCount += 1
        else:
            if lastMove == 'right':
                win.blit(playerStandRight, (x,y))
            else:
                win.blit(playerStandLeft, (x,y))

    for wolf in wolfs:
        wolf.drawAnimals(win)          
    for bullet in bullets:
        bullet.draw(win)

    if hp > 0:
        win.blit(aim, (pos[0]-50, pos[1]-50))
    
    pygame.display.update()
pos = [500, 500]
run = True
while run:



    clock.tick(FPS)

    if not T:
        S += 1
        if S >= FPS*0.5:
            S = 0
            T = True




    regen_time+=1
    if regen_time>FPS/2:
        regen_time = 0
        if hp<250:
            hp+=1




            
    scup_time+=1
    if scup_time>FPS/2:
        scup_time = 0
        Score+=1





    last_bonus+=1
    if last_bonus>FPS*0.6:
        r=random.randint(1,FPS)
        if r==1:
            heals.append(healb(random.randint(50, 1600), 0))
        if r==2:
            dds.append(ddb(random.randint(50, 1600), 0))
        if r==3:
            moons.append(moonb(random.randint(50, 1600), 0))
        if r<4:
            last_bonus=0




    if pygame.mouse.get_focused():
        pos = pygame.mouse.get_pos()






    b_color = (255, 200, 0)
    damage = 9
    TimeWolf = 4

    for dd in dds:
        dd.y+=dd.v
        if abs(dd.y-y+40)<60 and abs(dd.x-x)<100:
            dd.act = True
        if dd.act:
            dd.Ptime-=1
            damage = 18
            b_color = (255, 0, 0)
        if dd.Ptime==0:
            dds.pop(dds.index(dd))
        if dd.y > 760 and not dd.act:
            dds.pop(dds.index(dd))



    for moon in moons:
        moon.y+=moon.v
        if abs(moon.y-y+40) < 60 and abs(moon.x-x) < 100:
            moon.act = True
        if moon.act:
            moon.Ptime -= 1
            TimeWolf = 1.5
        if moon.Ptime == 0:
            moons.pop(moons.index(moon))
        if moon.y > 760 and not moon.act:
            moons.pop(moons.index(moon))


    for heal in heals:
        heal.y += heal.v
        if abs(heal.y - y + 40) < 60 and abs(heal.x - x) < 100:
            hp = min(hp + 15, MaxHp)
            heals.pop(heals.index(heal))
        elif heal.y > 760:
            heals.pop(heals.index(heal))
    







    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
            pygame.quit()
            
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 and hp > 0:

            if pos[0] > x + width // 2:
                lastMove = "right"
            else:
                lastMove = "left"
            if pos[0] != x + width // 2 or pos[1] != y + height // 2:
                cos = (pos[0] - (x + width // 2))/((pos[0] - (x + width // 2))**2 + (pos[1]-(y + height // 2))**2)**0.5
                sin = (pos[1]-(y + height // 2))/((pos[0] - (x + width // 2))**2 + (pos[1]-(y + height // 2))**2)**0.5
            vectorX = int(10*cos)
            vectorY = int(10*sin)
            if len(bullets) < max_kolvo_snaryads:
                bullets.append(Snaryad(round(x + width // 2),round(y + 140 // 2), 5, b_color, vectorX, vectorY, damage))











    if last_wolf > FPS*TimeWolf/3:
        ran = random.randint(1,2*FPS)
        if ran == 1:
            wolfs.append(Animal(1700, 740, 100, -1, False, 0))
            last_wolf = 0
        if ran == 2:
            wolfs.append(Animal(-300, 740, 100, 1, False, 0))
            last_wolf = 0

    last_wolf += 1





        
    for wolf in wolfs:
        if wolf.X > -400 and wolf.X < 1700: #and not wolf.attack:
            wolf.X += wolf.v * wolf.vektor
            if wolf.vektor == -1 and wolf.X + 238 + distAttack  < x:
                wolf.vektor = 1

            if wolf.vektor == 1 and wolf.X > x + width + distAttack:
                wolf.vektor = -1




            if (wolf.vektor == -1 and wolf.X < x + width and wolf.X > x) or\
                    (wolf.vektor == 1 and wolf.X + 238 + distAttack > x and wolf.X + 238 < x + width) and not wolf.attack:
                wolf.attack = True

            if wolf.attack and y <= 760 and y >= 760 - height and T and hp > 0:
                hp -= 50
                T = False
            if hp <= 0:
                pygame.mouse.set_visible(True)
                wolfs = []
                bullets = []
                moons = []
                heals = []
                dds =[]
                hp = MaxHp
                f = open("q.txt")
                best = f.read()
                best = max(int(best), Score)
                best = str(best)
                f.close()
                f = open("q.txt",'w')
                f.write(best)
                f.close()
                Score = 0
                game.menu()








    for bullet in bullets:
        deleted = False
        if bullet.x > 0 and bullet.x < 1671 and bullet.y > 0 and bullet.y < 1671:
            bullet.x += bullet.vectorX
            bullet.y += bullet.vectorY
        else:
            bullets.pop(bullets.index(bullet))



        for wolf in wolfs:
            if not deleted and bullet.x > wolf.X + pogr and bullet.x < wolf.X + 238 - pogr and bullet.y > wolf.Y + pogr and bullet.y < wolf.Y + 198 - pogr:
                bullets.pop(bullets.index(bullet))
                wolf.HP -= bullet.damag
                deleted = True
            if wolf.HP <= 0 and wolf in wolfs:
                wolfs.pop(wolfs.index(wolf))
                Score += 10
                #del (wolf)

        for bird in birds:
            if not deleted and bullet.x > bird.X + pogr and bullet.x <bird.X + 238 - pogr and bullet.y > bird.Y + pogr and bullet.y < bird.Y + 198 - pogr:
                bullets.pop(bullets.index(bullet))
                bird.HP -= bullet.damag
                deleted = True
            if bird.HP <= 0 and bird in birds:
                birds.pop(birds.index(bird))
                Score += 15
                

    

        

    keys = pygame.key.get_pressed()

    
        
    if keys[pygame.K_ESCAPE]:
        pygame.mouse.set_visible(True)
        game.menu()

    if keys[pygame.K_a] and hp > 0:
        if 5 < x:
            x -= speed
            left = True
            right = False
            lastMove = "left"
        elif x <= 5:
            left = True
            right = False
            lastMove = "left"
    elif keys[pygame.K_d] and hp > 0:
        if  x < 1671 - width - 5:
            x += speed
            right = True
            left = False
            lastMove = "right"
        elif x >= 1671 - width - 5:
            right = True
            left = False
            lastMove = "right"
    else:
        left = False
        right = False
        animCount = 0
    if not(jump):
        if keys[pygame.K_w]:
                jump = True

    else:
        if jump_count >= -10:
            if jump_count < 0:
                y += (jump_count ** 2) / 3
            else:
                y -= (jump_count ** 2) / 3
            jump_count -= 0.5

        else:
            jump = False
            jump_count = 10

    drawWindow()
